import ballerina/io;
import ballerina/http;

public function main() returns @tainted error? {
    http:Client httpClient = check new ("http://localhost:9090/learner");
    string learnerId = "";

    //create learners profile here
    io:println("\n___________POST request:___________----");
    json resp = check httpClient->post("/create", RawJson);
    result success = check resp.cloneWithType(result);
    learnerId = success.learnerId;
    io:println(resp.toJsonString());

    //get learner profile by id
    io:println("\n----------GET request:___________-------");
    json firstResp = check httpClient->get("/findLearnerById?learnerId=" + learnerId);
    io:println(firstResp.toJsonString());
    json Materials = check httpClient->get("/learningMaterials");
    
    io:println("\nLearning Materials");
    io:println(Materials.toJsonString());

    //update learner profile by id
    io:println("\n___________-PUT request:___________--------");
    Learner UpdateJson = check RawJson.cloneWithType(Learner);
    UpdateJson.firstname = "Arkin";
    UpdateJson.lastname = "Libuto";
    json UpdateResp = check httpClient->put("/update?learnerId=" + learnerId, UpdateJson.toJson());
    io:println(UpdateResp.toJsonString());

    //get all learners stored in memory
    io:println("\n___________--GET request:___________---------");
    json SecondResp = check httpClient->get("/findLearnerById?learnerId=" + learnerId);
    io:println(SecondResp.toJsonString());
}

final json RawJson = 
{
    "username": "JunLimbo", 
    "lastname": "Limbo", 
    "firstname": "Junior", 
    "preferred_formats": ["audio", "video", "text"], 
        "past_subjects": [
            {"course": "Algorithms", 
                "score": "B+"
            }, 
            {
                "course": "Programming I", 
                "score": "A+"
            }
        ]
};

type result record {
    string status;
    string learnerId;
};

public type Subjects record {|
    string course;
    string score;
|};

public type Learner record {|
    string username;
    string lastname;
    string firstname;
    string[] preferred_formats;
    Subjects[] past_subjects;
|};

